<?php
session_start();
$_SESSION["notif"];
?>
<!DOCTYPE html>
<html lang="fr_FR" class="no-js">

<head>
	<!-- Primary Meta Tags -->
	<title>AGENCE YWU - YES WE UP</title>
	<meta name="title" content="AGENCE YWU - YES WE UP">
	<meta name="description" content="Agence de communication 360 spécialisé dans le déploiement de solutions digitale pour entreprise et particulier">
	<meta name="author" content="YWU">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="/img/fav.png">

	<!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://www.facebook.com/yesweup/">
	<meta property="og:title" content="AGENCE YWU - YES WE UP">
	<meta property="og:description" content="Agence de communication 360 spécialisé dans le déploiement de solutions digitale pour entreprise et particulier">
	<meta property="og:image" content="https://ywudemo.000webhostapp.com/img/card_ywu.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:url" content="https://metatags.io/">
	<meta property="twitter:title" content="AGENCE YWU - YES WE UP">
	<meta property="twitter:description" content="Agence de communication 360 spécialisé dans le déploiement de solutions digitale pour entreprise et particulier">
	<meta property="twitter:image" content="https://ywudemo.000webhostapp.com/img/card_ywu.png">
	<!-- Meta mot clés -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Titre du site-->
	<title>Yes We Up</title>

	<!--	CSS	============================================= -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css">
	<link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="shortcut icon" href="#" type="image/png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.min.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/fontawesome-all.css">
	<link rel="stylesheet" type="text/css" href="css/icofont.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.lineProgressbar.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.theme.default.css">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="css/responsiveslides.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<!-- Facebook Messenger Live chat -->
</head>

<body>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v3.3'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/fr_FR/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
	 attribution=setup_tool
	 page_id="189880258612845"
	 theme_color="#2ECC71"
	 logged_in_greeting="Bienvenue ! Comment pouvons-nous vous aider ?"
	 logged_out_greeting="Bienvenue ! Comment pouvons-nous vous aider ?"
	 >
</div>


<div class="top-header hidden-xs hidden-sm">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="top-item-left">
						<ul>
							<li><a href="#"><i class="fa fa-envelope"></i> ywu.services@gmail.com</a></li>
							<li><a href="#"><i class="fa fa-phone"></i> +225 67502476</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="top-item-right">
						<ul>
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Debut Header-->
	<!-- ==== Header Start  ==== -->
	<header id="pageHeader" class="header navbar">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar icon-bar-last"></span>
				</button>
				<a class="navbar-brand" href="#">
					<img src="img/ywu_logo.png" alt="logo_ywu" />
				</a>
			</div>
			<nav id="myNavbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="#accueil">ACCUEIL</a></li>
					<li><a href="#services">NOTRE EXPERTISE</a></li>
					<!--<li><a href="#portfolio">PORTFOLIO</a></li>-->
                    <li><a href="#carriere">REJOIGNEZ-NOUS</a></li>
					<li><a href="#contacts">CONTACTS</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<!-- ==== Header End ==== -->
	<div id="accueil" class="main-header bg-opacity">
		<!-- main header -->
		<div id="particles-js"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="main-header-text">
						<h1>Propulsez votre communication</h1>
						<h3>Avec nos solutions de <span class="element"></span></h3>
						<a href="#" class="active">Notre Expertise</a>
						<a href="#">Contactez-Nous</a>
					</div>
				</div>
			</div>
		</div>
		<!-- main header -->
	</div>
	<!-- fin Header -->
    <!-- SECTION AGENCE -->
   <!-- <section id="about" class="pt-120 pb-90">
        <div class="container">
            <div class="row">
                <div class="section-title">
                    <div class="main-title">
                        <h1>L'AGENCE</h1>
                        <h4>QUI SOMMES-NOUS?</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="about-us-left">
                        <h4 class="about-title">UNE AGENCE 360<sup>o</sup> CREATIVE</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="about-img">
                        <img src="img/about.jpg" class="img-responsive" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!-- FIN SECTION AGENCE -->

	<!-- SECTION SERVICES -->
	<section id="services" class="pt-120 pb-90 services">
		<div class="container">
			<div class="row">
				<div class="section-title">
					<div class="main-title">
						<h1>NOS SERVICES</h1>
						<h4>NOTRE EXPERTISE</h4>
					</div>
					<p class="col-md-8 col-md-offset-2">
						YWU est une start-up informatique spécialisée dans le Marketing digitale.
						Nous proposons aux particuliers, petite/moyenne et grande entreprises des solutions Marketing
						taillées selon leurs besoins. Osez l'aventure !
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-sm-6">
					<div class="single-service text-center">
						<div class="icon">
							<img src="img/coding.svg" data-rjs="2" alt="">
						</div>

						<div class="content">
							<h4>Développement Web</h4>
							<p>Nous vous rapprochons de votre clientèle en développant des applications sur mésure pour faciliter
							à vos clients l'accès à vos services</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-sm-6">
					<div class="single-service text-center">
						<div class="icon">
							<img src="img/analytics.svg" data-rjs="2" alt="">
						</div>

						<div class="content">
							<h4>Marketing Digital</h4>
							<p>Grâce au digital, nous vous offrons l'opportunité d'accroître considérablement le nombre de vos
							prospects, et augmenter vos revenus</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-sm-6">
					<div class="single-service text-center">
						<div class="icon">
							<img src="img/art-design.svg" data-rjs="2" alt="">
						</div>

						<div class="content">
							<h4>Graphisme</h4>
							<p>Nous faisons de vos rêves une réalité grâce à méthode d'approche 360<sup>o</sup> pour traduire vos services
							en moyens de communication visuelle</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- FIN SECTION SERVICES -->

	<!-- SECTION CLIENTS -->
	<section id="client" class="padding-60">
		<div class="container">
			<div class="row">
				<div class="section-title">
					<div class="main-title">
						<h1>NOS R&Eacute;F&Eacute;RENCES TECHNOLOGIQUES</h1>
						<h4>TECHNOLOGIES UTILIS&Eacute;ES</h4>
					</div>
			</div>
			<div class="row">
				<div id="client-caro" class="clients-carousel  owl-carousel">
					<div class="item">
						<a href="#"><img src="img/ywu_logo.png" class="img-responsive" alt=""></a>
					</div>
					<div class="item">
						<a href="#"><img src="img//dummy-image.png" class="img-responsive" alt=""></a>
					</div>
					<div class="item">
						<a href="#"><img src="img/clients/dummy-image.png" class="img-responsive" alt=""></a>
					</div>
					<div class="item">
						<a href="#"><img src="img/clients/dummy-image.png" class="img-responsive" alt=""></a>
					</div>
					<div class="item">
						<a href="#"><img src="img/clients/dummy-image.png" class="img-responsive" alt=""></a>
					</div>
					<div class="item">
						<a href="#"><img src="img/clients/dummy-image.png" class="img-responsive" alt=""></a>
					</div>
					<div class="item">
						<a href="#"><img src="img/clients/dummy-image.png" class="img-responsive" alt=""></a>
					</div>
					<div class="item">
						<a href="#"><img src="img/clients/dummy-image.png" class="img-responsive" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- FIN  SECTION CLIENT -->

	<!-- SECTION TECHNOLOGIES -->
	<!--<section  id="portfolio" class="padding-120">
	<div class="container">
		<div class="row">
			<div class="section-title">
				<div class="main-title">
					<h1>OUTILS</h1>
					<h4>Technologies utilisées</h4>
				</div>
				<p class="col-md-8 col-md-offset-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.</p>
			</div>
		</div>
		<div class="row">
			<div  id="portfolio-slider" class="owl-carousel project-carousel">
				<div class="project-item">
					<div class="box">
						<img src="image/portfolio/dummy-image.jpg" alt="">
						<div class="box-content">
							<div class="inner-content">
								<h3 class="title">Development</h3>
								<span class="post">Web Developer</span>
							</div>
							<ul class="icon">
								<li><a href="image/portfolio/dummy-image.jpg" class="project-popup"><i class="fa fa-search"></i></a></li>
								<li><a href="#"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="project-description">
						<h4>Lorem ipsum dolor sit amet</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
					</div>
				</div>
				<div class="project-item">
					<div class="box">
						<img src="image/portfolio/dummy-image.jpg" alt="">
						<div class="box-content">
							<div class="inner-content">
								<h3 class="title">Development</h3>
								<span class="post">Web Developer</span>
							</div>
							<ul class="icon">
								<li><a href="image/blog/2.jpeg" class="project-popup"><i class="fa fa-search"></i></a></li>
								<li><a href="#"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="project-description">
						<h4>Lorem ipsum dolor sit amet</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
					</div>
				</div>
				<div class="project-item">
					<div class="box">
						<img src="image/portfolio/dummy-image.jpg" alt="">
						<div class="box-content">
							<div class="inner-content">
								<h3 class="title">Development</h3>
								<span class="post">Web Developer</span>
							</div>
							<ul class="icon">
								<li><a href="image/blog/3.jpeg" class="project-popup"><i class="fa fa-search"></i></a></li>
								<li><a href="#"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="project-description">
						<h4>Lorem ipsum dolor sit amet</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
					</div>
				</div>
				<div class="project-item">
					<div class="box">
						<img src="image/portfolio/dummy-image.jpg" alt="">
						<div class="box-content">
							<div class="inner-content">
								<h3 class="title">Development</h3>
								<span class="post">Web Developer</span>
							</div>
							<ul class="icon">
								<li><a href="image/blog/4.jpeg" class="project-popup"><i class="fa fa-search"></i></a></li>
								<li><a href="#"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="project-description">
						<h4>Lorem ipsum dolor sit amet</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
					</div>
				</div>
				<div class="project-item">
					<div class="box">
						<img src="image/portfolio/dummy-image.jpg" alt="">
						<div class="box-content">
							<div class="inner-content">
								<h3 class="title">Development</h3>
								<span class="post">Web Developer</span>
							</div>
							<ul class="icon">
								<li><a href="image/blog/5.jpeg" class="project-popup"><i class="fa fa-search"></i></a></li>
								<li><a href="#"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="project-description">
						<h4>Lorem ipsum dolor sit amet</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>-->
	<!-- FIN SECTION TECHNOLOGIES -->

	<!-- SECTION REJOIGNEZ-NOUS -->
	<section id="carriere" class="padding-60 bg-request bg-opacity">
		<div class="container">
			<div class="row">
				<div class="work-with text-center">
					<h4>ENVIE DE FAIRE PARTIR DE L'&Eacute;QUIPE?</h4>
					<p>Vous souhaitez participer à une aventure au delà des limites</p>
					<a href="#">REJOIGNEZ NOUS</a>
				</div>
			</div>
		</div>
	</section>
	<!-- FIN SECTION REJOIGNEZ-NOUS-->

	<!-- SECTION CONTACTS-->

	<!-- FIN SECTION CONTACTS-->
	<section id="contacts" class="padding-60">
		<div class="container">
			<div class="row">
				<div class="section-title">
					<div class="main-title">
						<h1>QU'ATTENDEZ-VOUS?</h1>
						<h4>CONTACTEZ-NOUS</h4>
					</div>
					<p class="col-md-8 col-md-offset-2">Besoin d'un service particulier, nous sommes à votre entière disposition.
						Vous avez juste à remplir le formulaire ci-dessous </p>
				</div>
				<div class="contact-form">
					<form class="contact-form" action="mail.php" method="post">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="name" placeholder="Nom Complet / Nom de L'entreprise..." required="required">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="email" class="form-control" name="email" placeholder="Adresse email..." required="required"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="telephone" placeholder="Numéro de téléphone..." required="required" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="objet" placeholder="Objet du Message..." required="required">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<textarea class="form-control" name="message" placeholder="À vous de jouer..."></textarea>
								</div>
							</div>
							<div class="col-md-12 text-center">
								<button class="btn submit-btn"><i class="fa fa-paper-plane"></i> ENVOYER</button>
							</div>
						</div>
					</form>
					<br/>
                    <span id="notification"></span>
					<?php
                   function afficherNotif(){
                        if ($_SESSION["notif"] == "success"){
                            echo '
                                <div class="statut-message alert alert-success alert-dismissible" role="alert">
				                    <strong>C\'est Fait!</strong> Nous avons bien reçu votre message!!
					                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						                <span aria-hidden="true">&times;</span>
					                </button>
			                    </div>
                            ';
                            $_SESSION["notif"] = "echec";
                        }else{
                            echo "";
                        }
                   }

                    afficherNotif();
                    ?>
				</div>
			</div>
		</div>
	</section>
	<!-- PIED DE PAGE -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-3 footer-logo">
					<a class="" href="#">
						<img src="img/ywu_logo.png" alt="">
					</a>
					<p>Agence de communication 360 spécialisé
						dans le déploiement de solutions digitale
						pour entreprise et particulier</p>
					<ul>
						<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
				</div>
				<div class="col-md-6 footer-links">
					<h5 class="footer-title">Plan du site</h5>
					<ul class="footer-link">
						<li><a href="#accueil"><i class="fas fa-angle-double-right"></i> ACCUEIL</a></li>
						<li><a href="#services"><i class="fas fa-angle-double-right"></i> NOTRE EXPERTISE</a></li>
						<li><a href="#portfolio"><i class="fas fa-angle-double-right"></i> PORTFOLIO</a></li>
						<li><a href="#contact"><i class="fas fa-angle-double-right"></i> CONTACTS</a></li>
						<li><a href="#carriere"><i class="fas fa-angle-double-right"></i> REJOIGNEZ-NOUS</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-news">
					<h5 class="footer-title">Restez Informez</h5>
					<p>Restez à l'écoute, nous nous rapprochons encore plus de vous</p>
					<div class="newsletter">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Votre adresse email" aria-label="Search for..." autocomplete="off">
							<span class="input-group-btn">
                      	<button class="btn newsletter-btn" type="button"><i class="fa Example of paper-plane fa-paper-plane"></i></button>
                    	</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="footer-down">
		<div class="container">
			<div class="row text-center">
				<div class="copyright-text">
					<p>© Copyright 2019. Tous droits réservés. <a class="footer-team" href="#">YesWeUP</a></p>
				</div>
			</div>
		</div>
	</div>
	<!-- FIN PIED DE PAGE ==== -->

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.parallax-1.1.3.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/isotope.js"></script>
	<script src="js/particles.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/responsiveslides.js"></script>
	<script src="js/jquery.lineProgressbar.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/typed.js"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZtTj2tvwsorp_DNPh3ZN_pMlSniet5h4&callback=initMap"></script>
	<script src="js/jquery.counterup.min.js"></script>
	<script src="js/masonry.pkgd.js"></script>
	<script src="js/tweetie.js"></script>
	<script src="js/main.js"></script>
	<script>
        /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
        particlesJS.load('particles-js', 'assets/particles.json', function() {
            console.log('callback - particles.js config loaded');
        });
	</script>
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d3f28b9873e15af"></script>

</body>

</html>
